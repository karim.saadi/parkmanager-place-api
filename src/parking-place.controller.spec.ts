import { Test, TestingModule } from '@nestjs/testing';
import { ParkingPlaceController } from './parking-place.controller';
import { ParkingPlaceService } from './parking-place.service';

describe('ParkingPlaceController', () => {
  let parkingPlaceController: ParkingPlaceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ParkingPlaceController],
      providers: [ParkingPlaceService],
    }).compile();

    parkingPlaceController = app.get<ParkingPlaceController>(ParkingPlaceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(parkingPlaceController.getHello()).toBe('Hello World!');
    });
  });
});
