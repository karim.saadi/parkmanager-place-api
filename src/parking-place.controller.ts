import { Controller, Get } from '@nestjs/common';
import { ParkingPlaceService } from './parking-place.service';

@Controller()
export class ParkingPlaceController {
  constructor(private readonly parkingPlaceService: ParkingPlaceService) {}

  @Get()
  getHello(): string {
    return this.parkingPlaceService.getHello();
  }
}
