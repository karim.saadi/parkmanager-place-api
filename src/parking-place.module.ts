import { Module } from '@nestjs/common';
import { ParkingPlaceController } from './parking-place.controller';
import { ParkingPlaceService } from './parking-place.service';

@Module({
  imports: [],
  controllers: [ParkingPlaceController],
  providers: [ParkingPlaceService],
})
export class ParkingPlaceModule {}
